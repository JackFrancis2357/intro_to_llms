# Intro_to_LLMs

## Setup your environment

In a terminal, setup your virtual environment and install the requirements in `requirements.txt`.

Both of the commands below assume that the keyword `python` defaults to your Python version of choice.

On Windows:

```shell
python -m venv .venv/
.venv/Scripts/activate
pip install -r requirements.txt
```

On Linux/Mac:

```shell
python -m venv .venv/
source .venv/bin/activate
pip install -r requirements.txt
```

## Download the Word2Vec model

Load the Google News Word2Vec model
Can get the model here: https://github.com/mmihaltz/word2vec-GoogleNews-vectors, click the "GoogleNews-vectors-negative300.bin.gz" link in the README. File is ~1.5GB
