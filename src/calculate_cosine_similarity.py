import numpy as np


# Calculate cosine similarity
def calc_cos_sim(v1, v2):
    cos_sim = np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))
    return cos_sim


# Sentences
s1 = np.array([1, 2, 3, 4, 5, 0])
s2 = np.array([1, 2, 6, 3, 4, 5])
s3 = np.array([5, 2, 7, 0, 0, 0])
s4 = np.array([8, 4, 9, 5, 0, 0])

# Check out the effect of sorting
# s1 = sorted(s1)
# s2 = sorted(s2)
# s3 = sorted(s3)
# s4 = sorted(s4)

s1_s2_sim = calc_cos_sim(s1, s2)
s1_s3_sim = calc_cos_sim(s1, s3)
s1_s4_sim = calc_cos_sim(s1, s4)
s2_s3_sim = calc_cos_sim(s2, s3)
s2_s4_sim = calc_cos_sim(s2, s4)
s3_s4_sim = calc_cos_sim(s3, s4)

print(f"{s1_s2_sim=:.2f}")
print(f"{s1_s3_sim=:.2f}")
print(f"{s1_s4_sim=:.2f}")
print(f"{s2_s3_sim=:.2f}")
print(f"{s2_s4_sim=:.2f}")
print(f"{s3_s4_sim=:.2f}")
