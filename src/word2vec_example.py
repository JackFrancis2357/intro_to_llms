from gensim.models import KeyedVectors
from gensim.parsing.preprocessing import remove_stopwords
import numpy as np

# Load the Google News Word2Vec model
# Can get the model here: https://github.com/mmihaltz/word2vec-GoogleNews-vectors, click the "GoogleNews-vectors-negative300.bin.gz" link in the README. File is ~1.5GB
model = KeyedVectors.load_word2vec_format("./GoogleNews-vectors-negative300.bin.gz", binary=True)


def preprocess(sentence):
    # Basic preprocessing: remove stopwords, lowercasing, and split into words
    return remove_stopwords(sentence.lower()).split()


def sentence_vector(sentence, model):
    words = preprocess(sentence)
    word_vectors = [model[word] for word in words if word in model]
    if len(word_vectors) == 0:
        return np.zeros(model.vector_size)
    return np.mean(word_vectors, axis=0)


def calc_cos_sim(vec1, vec2):
    return np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))


# Example sentences
sentence_1 = "That was a good dinner."
sentence_2 = "That was not a good dinner."
sentence_3 = "Dinner was fantastic."
sentence_4 = "I felt good after dinner."


# Calculate sentence vectors
vec1 = sentence_vector(sentence_1, model)
vec2 = sentence_vector(sentence_2, model)
vec3 = sentence_vector(sentence_3, model)
vec4 = sentence_vector(sentence_4, model)


# Calculate similarity
similarity = calc_cos_sim(vec1, vec2)
print(f"Similarity: {similarity}")

s1_s2_sim = calc_cos_sim(vec1, vec2)
s1_s3_sim = calc_cos_sim(vec1, vec3)
s1_s4_sim = calc_cos_sim(vec1, vec4)
s2_s3_sim = calc_cos_sim(vec2, vec3)
s2_s4_sim = calc_cos_sim(vec2, vec4)
s3_s4_sim = calc_cos_sim(vec3, vec4)

print(f"{s1_s2_sim=:.2f}")
print(f"{s1_s3_sim=:.2f}")
print(f"{s1_s4_sim=:.2f}")
print(f"{s2_s3_sim=:.2f}")
print(f"{s2_s4_sim=:.2f}")
print(f"{s3_s4_sim=:.2f}")
